﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Triplefin.CRM.API.Models
{
    public class ServiceEvent
    {
    }
    public class UpdateServiceEventResponse
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
    }
}