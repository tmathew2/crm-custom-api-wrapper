﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Triplefin.CRM.API.Models
{
    public class Enrollment
    {
        public Guid Id { get; set; }
        public Patient Patient { get; set; }
        public ClinicalInformation ClinicalInformation { get; set; }
        public Prescription Prescription { get; set; }
        public Insurance Insurance { get; set; }
    }
}