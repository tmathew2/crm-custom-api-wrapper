﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Triplefin.CRM.API.Models;

namespace Triplefin.CRM.API.Logic
{
    public class PatientsLogic : LogicBase
    {
        public static List<Patient> GetBySearch(string firstName, string lastName)
        {
            List<Patient> patients = new List<Patient>();

            try
            {
                using (ServiceContext context = new ServiceContext(Service))
                {
                    IQueryable<Contact> query;

                    query = (from c in context.ContactSet
                                 where c.FirstName.Contains(firstName) || c.LastName.Contains(lastName)
                                 select c).Take(ListLimit);
                    foreach (var p in query)
                    {
                        patients.Add(new Patient()
                        {
                            Address1 = p.Address1_Line1,
                            Address2 = p.Address1_Line2,
                            City = p.Address1_City,
                            FirstName = p.FirstName,
                            Id = p.Id,
                            LastName = p.LastName,
                            State = p.Address1_StateOrProvince,
                            Zip = p.Address1_PostalCode,
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogic.Create("PatientsLogic:GetBySearch", ex.ToString());
            }
            return patients;
        }
        public static Patient GetById(Guid id)
        {
            Patient patient = null;
            try
            {
                using (ServiceContext context = new ServiceContext(Service))
                {
                    var p = (from c in context.ContactSet
                             where c.Id.Equals(id)
                             select c).FirstOrDefault();
                    if (null != p)
                    {
                        patient = new Patient()
                        {
                            Address1 = p.Address1_Line1,
                            Address2 = p.Address1_Line2,
                            City = p.Address1_City,
                            FirstName = p.FirstName,
                            Id = p.Id,
                            LastName = p.LastName,
                            State = p.Address1_StateOrProvince,
                            Zip = p.Address1_PostalCode,
                        };
                    }
                }
            }
            catch(Exception ex)
            {
                ErrorLogic.Create("PatientsLogic:GetById", ex.ToString());
            }
            return patient;
        }
    }
}