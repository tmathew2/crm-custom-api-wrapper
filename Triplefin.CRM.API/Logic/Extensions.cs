﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace Triplefin.CRM.API.Logic
{
    public static class Extensions 
    {
        public static int AsInt(this object o)
        {
            int i = 0;
            if (null != o)
            {
                int.TryParse(o.ToString(), out i);
            }
            return i;
        }
        public static object StringToDBNull(string value)
        {
            if (!string.IsNullOrEmpty(value))
                return value;
            return DBNull.Value;
        }
        public static long AsLong(this object o)
        {
            long l = 0;
            if (null != o)
            {
                long.TryParse(o.ToString(), out l);
            }
            return l;
        }
        public static decimal AsDecimal(this object o)
        {
            decimal d = 0;
            if (null != o)
            {
                decimal.TryParse(o.ToString(), out d);
            }
            return d;
        }
        public static bool AsBool(this object o)
        {
            bool b = false;
            if (null != o)
            {
                bool.TryParse(o.ToString(), out b);
            }
            return b;
        }
        public static DateTime AsDateTime(this object o)
        {
            DateTime d;
            if (null == o)
            {
                d = DateTime.MinValue;
            }
            else
            {
                DateTime.TryParse(o.ToString(), out d);
            }
            return d;
        }
        public static DateTime? MakeNullIfMinValue(this DateTime o)
        {
            DateTime? d = null;
            if (o != DateTime.MinValue)
            {
                d = o;
            }
            return d;
        }
        public static SqlDateTime? MakeSqlNullIfMinValue(this DateTime o)
        {
            SqlDateTime? d = null;
            if (o != DateTime.MinValue)
            {
                d = o;
            }
            return d;
        }
    }
}