﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Description;
using System.Web;

namespace Triplefin.CRM.API.Logic
{
    public class LogicBase
    {
        private static string ServiceUrl { get { return ConfigurationManager.AppSettings["ServiceUrl"]; } }
        private static string Username { get { return ConfigurationManager.AppSettings["Username"]; } }
        private static string Password { get { return ConfigurationManager.AppSettings["Password"]; } }
        public static int ListLimit { get { return ConfigurationManager.AppSettings["ListLimit"].AsInt(); } }
        public static IOrganizationService Service
        {
            get
            {
                try
                {
                    ClientCredentials credentials = new ClientCredentials();
                    credentials.UserName.UserName = Username;
                    credentials.UserName.Password = Password;
                    Uri serviceUri = new Uri(ServiceUrl);
                    OrganizationServiceProxy proxy = new OrganizationServiceProxy(serviceUri, null, credentials, null);
                    proxy.EnableProxyTypes();
                    return (IOrganizationService)proxy;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
    }
}