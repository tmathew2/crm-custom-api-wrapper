﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Triplefin.CRM.API.Logic;
using Triplefin.CRM.API.Models;

namespace Triplefin.CRM.API.Controllers
{
    [RoutePrefix("api/serviceevents/v1")]
    public class ServiceEventsController : ApiController
    {
        [HttpPost, Route("update")]
        public UpdateServiceEventResponse UpdateServiceEvent(ServiceEvent serviceEvent)
        {
            return ServiceEventsLogic.UpdateServiceEvent(serviceEvent);
        }
    }
}
