﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Triplefin.CRM.API.Logic;
using Triplefin.CRM.API.Models;

namespace Triplefin.CRM.API.Controllers
{
    [RoutePrefix("api/patients/v1")]
    public class PatientsController : ApiController
    {
        [HttpGet, Route("search")]
        public IEnumerable<Patient> search(string firstname, string lastname)
        {
            return PatientsLogic.GetBySearch(firstname, lastname);
        }

        [HttpGet, Route("patient")]
        public Patient patient(Guid id)
        {
            return PatientsLogic.GetById(id);
        }        
    }
}
